#!/usr/bin/env bash

for f in $(find . -name '*.md')
do
  n=$(grep '```yaml' $f | wc -l)
  if [ $n -ne "0" ]
  then
    # echo "Found $n yaml codeblocks in file: $f"
    grep -nH '```yaml' $f | cut -f1,2 -d: > line_numbers.txt
    readarray -t LINE_ARRAY < line_numbers.txt
    for (( i=1; i <= $n; ++i ))
    do
      awk -v count=$i '/```yaml$/&&++k==count,/```$/' $f | grep -v '```' | yamllint -
      if [ $? -ne 0 ]
      then
        echo "YAML lint error found in codeblock ${LINE_ARRAY[i-1]}"
      fi
    done
  fi
done
